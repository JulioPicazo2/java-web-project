package com.softtek.academy.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.softtek.academy.web.dao.MomentumDAO;
import com.softtek.academy.web.model.Momentum;

@Service
public class MomentumServiceImpl implements MomentumService{
	@Autowired
	@Qualifier("colorRepository")
	private MomentumDAO momentumDao;
	
	public MomentumServiceImpl(MomentumDAO colorDao) {
		this.momentumDao = momentumDao;
	}
	
	@Override
	public Momentum getById(int id) {
		// TODO Auto-generated method stub
		return momentumDao.getById(id);
	}


	@Override
	public List<Momentum> getAll() {
		return momentumDao.getAll();
	}

	@Override
	public List<Momentum> getFilterList() {
		List<Momentum> colors = momentumDao.getAll();
		colors.removeIf((c) -> c.getName().equals("Black"));
		
		return colors;
	}

}
