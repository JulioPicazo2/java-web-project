package com.softtek.academy.web.service;

import java.util.List;

import com.softtek.academy.web.model.Momentum;

public interface MomentumService {
    List<Momentum> getAll();
	
    Momentum getById(int id);

	List<Momentum> getFilterList();	
}
