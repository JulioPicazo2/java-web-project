package com.softtek.academy.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.softtek.academy.web.model.Momentum;
import com.softtek.academy.web.service.MomentumService;


@RestController
@RequestMapping("api/v1")
public class MomentumController {
	@Autowired
	MomentumService momentumService;
	
	@RequestMapping(value = "users", method = RequestMethod.GET)
	public List<Momentum> colors() {
		return momentumService.getAll();
	}
	
	@RequestMapping(value = "users/{id}", method = RequestMethod.GET)
	public Momentum color(@PathVariable int id) {
		return momentumService.getById(id);
	}
}
