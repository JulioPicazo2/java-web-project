package com.softtek.academy.web.dao;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.softtek.academy.web.model.Momentum;

public class MomentumJDBCImpl implements MomentumDAO{
private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Momentum> getAll() {
		String SQL_SELECT = "Select * from momentum";
		List<Momentum> result = jdbcTemplate
				.query(SQL_SELECT, (rs, rowNum) -> {
					Momentum color = new Momentum(
					rs.getInt("num"),
					rs.getString("department"),
					rs.getString("name"),
					rs.getInt("id"),
					rs.getDate("date"),
					rs.getString("clockType"),
					rs.getInt("deviceId")
					);
			return color;
		});
		return result;
	}

	@Override
	public Momentum getById(int id) {
		String SQL_SELECT = "Select * from momentum where num = ?";
		Momentum color = jdbcTemplate
//				.queryForObject(SQL_SELECT, 
//						new Object[] {id}, (rs, rowNum) -> {
//							Color c = new Color(
//									rs.getLong("colorId"),
//									rs.getString("name"),
//									rs.getString("hexValue")
//									);
//							return c;
//						});
				.queryForObject(SQL_SELECT, new Object[] {id},
						new BeanPropertyRowMapper<Momentum>(Momentum.class));
		return color;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

}
