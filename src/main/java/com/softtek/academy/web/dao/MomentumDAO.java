package com.softtek.academy.web.dao;

import java.util.List;

import com.softtek.academy.web.model.Momentum;

public interface MomentumDAO {
	List<Momentum> getAll();

	Momentum getById(int id);
}
