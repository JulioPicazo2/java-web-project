package com.softtek.academy.web.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.softtek.academy.web.dao.MomentumDAO;
import com.softtek.academy.web.model.Momentum;

@Repository("momentumRepository")
public class MomentumRepository implements MomentumDAO{

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Momentum> getAll() {
		return (List<Momentum>)entityManager
					.createQuery("Select c from Momentum c", Momentum.class).getResultList();
	}

	@Override
	public Momentum getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}
}
